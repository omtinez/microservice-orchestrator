"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const microservice_1 = require("microservice");
const ootils_1 = require("ootils");
const minimist = require("minimist");
const url = require("url");
// Main entrypoint
function main(opts = {}) {
    return __awaiter(this, void 0, void 0, function* () {
        // Parse command-line arguments
        const args = minimist(process.argv.slice(1));
        // Helper function used to read arguments
        function getopt(name) {
            return opts[name] || args[name];
        }
        // Parse command-line arguments and define parameters
        const GLOBAL_STATS = {};
        const MAX_STATUS_RETRY = 5;
        const SERVICE_EXPIRATION_HOURS = getopt('service-expiration-hours') || 1;
        // Initialize microservice
        const service = yield microservice_1.MicroService.create(getopt('db'));
        // Setup debugging
        process.on('unhandledRejection', err => service.log('E', err));
        // Keep an index of the last triggered service for each path to allow round-robin selection
        const servicesIndex = {};
        // Helper function to request a status from a single service given the status path
        function serviceStatus(serv, retried = 0) {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                const query = url.format({ query: { date: new Date().getTime() } });
                service.log('I', 'Requesting status for service at ' + serv.status_url + query);
                try {
                    const res = JSON.parse(yield ootils_1.Requests.get(serv.status_url + query));
                    const status = res.data;
                    const msg = `Service ${serv.name}${serv.path} responded with status: ` +
                        JSON.stringify(status).substring(0, 50) + '...';
                    service.log('V', msg);
                    yield service.db.run(`
                    UPDATE services SET status_last=(?), status_timestamp=datetime('now')
                    WHERE status_url=(?)`, [JSON.stringify(status), serv.status_url]);
                    resolve(res.data);
                }
                catch (err) {
                    const msg = `Error querying service ${serv.name}${serv.path} for status. `;
                    service.log('E', msg, err);
                    if (retried < MAX_STATUS_RETRY) {
                        setTimeout(() => {
                            serviceStatus(serv, retried++).then(resolve).catch(reject);
                        }, 1000 * Math.pow(3, retried + 1));
                    }
                    else if (err.code === 'ECONNREFUSED') {
                        service.log('E', msg, err.message);
                        reject(new Error(msg + err.message));
                    }
                    else {
                        service.log('E', msg, err.message);
                        reject(new Error(msg + err.message));
                    }
                }
            }));
        }
        // Helper function to request a status from all services and delete stale ones
        function cleanupServices(expiration_hours = getopt('service-expiration-hours')) {
            return __awaiter(this, void 0, void 0, function* () {
                // Delete expired services
                if (expiration_hours && expiration_hours > 0) {
                    yield service.db.run(`
                DELETE FROM services WHERE
                timestamp < datetime('now', '-${expiration_hours} hours')`);
                }
                // Delete services with non-successfull status
                const rows = yield service.db.all("SELECT * FROM services");
                if (!rows || !rows.length) {
                    const msg = 'No active services to delete';
                    service.log('V', msg);
                }
                else {
                    // Keep track of the services being pinged for status, since one status endpoint
                    // may belong to multiple services
                    const status_urls = [];
                    rows.forEach((row) => __awaiter(this, void 0, void 0, function* () {
                        const serv = row;
                        if (status_urls.indexOf(serv.status_url) === -1) {
                            status_urls.push(serv.status_url);
                            try {
                                yield serviceStatus(serv);
                            }
                            catch (err) {
                                yield service.db.run(`DELETE FROM services WHERE status_url = ?`, [serv.status_url]);
                                const msg = `Service ${serv.name} successfully deleted from database`;
                                service.log('V', msg);
                            }
                        }
                    }));
                }
            });
        }
        // Initialize services database
        service.db.run(`
        CREATE TABLE IF NOT EXISTS services (
            name              TEXT NOT NULL,
            path              TEXT NOT NULL,
            spec              TEXT NOT NULL,
            redirect_url      TEXT NOT NULL,
            status_url        TEXT DEFAULT NULL,
            status_last       TEXT DEFAULT NULL,
            status_timestamp  DATETIME DEFAULT CURRENT_TIMESTAMP,
            timestamp         DATETIME DEFAULT CURRENT_TIMESTAMP,
            UNIQUE (name, path, redirect_url))`);
        // Routes setup
        service.route('/register', (request, response) => __awaiter(this, void 0, void 0, function* () {
            // Make sure that path begins with a slash
            const name = request.query.name;
            const spec = request.query.spec;
            const prependSlash = (path) => '/' + path.substring(path[0] === '/' ? 1 : 0);
            const local_path = prependSlash(request.query.local_path); // path in service
            const remote_path = prependSlash(request.query.remote_path); // path in orchestrator
            // Extract the remote address from headers or incoming socket connection
            const remote = (request.headers['x-forwarded-for'] || request.connection.remoteAddress).toString();
            // Create the URL piece by piece
            const urlParts = {};
            urlParts.port = request.query.port;
            urlParts.pathname = local_path;
            urlParts.protocol = request.protocol;
            if (request.query.redirect_url) {
                // Allow for host to register foreign services... TODO: review
                const parts = url.parse(request.query.redirect_url);
                urlParts.hostname = parts.hostname;
                urlParts.protocol = parts.protocol || 'http';
                urlParts.port = parts.port || request.query.port;
            }
            else {
                urlParts.hostname = remote;
            }
            // Reuse the same URL for status but changing the pathname only
            const redirect_url = url.format(urlParts);
            urlParts.pathname = 'status';
            const status_url = url.format(urlParts);
            // Check if service is already in DB
            try {
                const row = yield service.db.get(`
                SELECT COUNT(*) AS ct FROM services WHERE name = ? AND path = ? AND redirect_url = ?`, [name, remote_path, redirect_url]);
                if (row.ct > 0) {
                    const msg = 'Service already registered';
                    // TODO: more appropriate response code
                    response.status(200).send({ status: 'OK', data: msg });
                    service.log('V', msg);
                }
                else {
                    // Before inserting service into database, query status
                    try {
                        const status = yield serviceStatus({
                            name: name, path: remote_path, status_url: status_url
                        });
                        yield service.db.run(`
                            INSERT INTO services (name, path, spec, redirect_url, status_url, status_last)
                            VALUES (?, ?, ?, ?, ?, ?)`, [name, remote_path, spec, redirect_url, status_url, status]);
                        const url_from = '/service/' + request.query.name + remote_path;
                        const msg = 'Service registered successfully: ' + url_from + ' -> ' + redirect_url;
                        response.send({ status: 'OK', data: msg });
                        service.log('I', msg);
                    }
                    catch (err) {
                        const msg = 'Error occurred when registering service "' + request.query.name + '"';
                        response.status(500).send({ status: 'ERR', data: msg });
                        service.log('E', msg, err.message);
                    }
                }
            }
            catch (err) {
                const msg = `Unable to query services table`;
                service.log('E', msg, err.message);
                response.status(500).send({ status: 'ERR', data: msg });
            }
        }), { method: 'GET', mandatoryQueryParameters: ['name', 'port', 'local_path', 'remote_path', 'spec'] });
        service.route('/status', (request, response) => __awaiter(this, void 0, void 0, function* () {
            try {
                const rows = yield service.db.all(`
                SELECT name, path, redirect_url, status_last, status_timestamp FROM services`);
                rows.forEach((row) => {
                    try {
                        row.status_last = JSON.parse(row.status_last);
                    }
                    catch (ex) { }
                });
                GLOBAL_STATS.services = rows;
                response.send({ status: 'OK', data: GLOBAL_STATS });
            }
            catch (err) {
                const msg = 'Unable to retrieve status from database';
                response.send({ status: 'ERR', data: msg });
                service.log('E', msg, err.message);
            }
        }));
        service.route('/service/:name/:path?', (request, response) => __awaiter(this, void 0, void 0, function* () {
            const name = request.params.name;
            const path = '/' + (request.params.path || '');
            GLOBAL_STATS.requests_served++;
            try {
                const rows = yield service.db.all(`SELECT * FROM services WHERE name = ? AND path = ?`, [name, path]);
                if (!rows || !rows.length) {
                    GLOBAL_STATS.requests_failed++;
                    const msg = `Registered service not found: ${name}${path}`;
                    response.status(404).send({ status: 'ERR', data: msg });
                    service.log('E', msg);
                }
                else {
                    const key = name + ',' + path;
                    if (!servicesIndex[key]) {
                        servicesIndex[key] = 0;
                    }
                    const index = servicesIndex[key];
                    const serv = rows[index];
                    const redirect_url = serv.redirect_url + url.format({ query: request.query });
                    servicesIndex[key] = (index + 1) % rows.length;
                    service.log('V', `Sending "${name}" to ${redirect_url} (${index})`);
                    // TODO: allow for redirect instead of rewrite
                    //response.redirect(redirect_url);
                    const res = yield ootils_1.Requests.get(redirect_url, { resolveWithFullResponse: true, simple: false });
                    // Relay the status code, headers and response
                    response.status(res.statusCode).header(res.headers).send(res.body);
                    GLOBAL_STATS.requests_success++;
                }
            }
            catch (err) {
                GLOBAL_STATS.requests_failed++;
                // If we can't connect to the service, attempt to unregister immediately TODO
                if (err.code === 'ECONNREFUSED') {
                    const msg = 'Service refused to connect: ' + err.message;
                    response.status(500).send(msg);
                    service.log('E', msg);
                }
                else {
                    const msg = 'Error requesting service: ' + err.message;
                    response.status(500).send(msg);
                    service.log('E', msg);
                }
            }
        }));
        // Maintenance loop
        const maintenance_loop = () => {
            cleanupServices();
            service.clearCache();
            GLOBAL_STATS.requests_per_second = GLOBAL_STATS.requests_served / 60.0;
            GLOBAL_STATS.requests_served = 0;
            GLOBAL_STATS.requests_failed = 0;
            GLOBAL_STATS.requests_success = 0;
        };
        setInterval(maintenance_loop, 60000);
        // Pick a random port
        service.start(getopt('port') || Math.floor(Math.random() * 8999) + 1000);
        // Signal parent process that server has started by disconnecting from the IPC channel
        if (process.connected) {
            process.disconnect();
        }
        return service;
    });
}
exports.main = main;
// If starting from the command line, begin execution
if (typeof require !== 'undefined' && require.main === module) {
    main();
}
