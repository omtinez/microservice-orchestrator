'use strict';

const fs = require('fs');
const pckg = require('./package.json');
const args = require('minimist')(process.argv.slice(1));

// Installs this microservice as a systemd service, requires root

// Check if package was installed with global flag
if (!args.output && process.env.npm_config_argv &&
    JSON.parse(process.env.npm_config_argv).cooked.indexOf('--global') === -1) {
    console.log('Skipping installation because --global flag was not used');
    process.exit(0);
}

// Ensure we are running as root
if (process.getuid() !== 0) {
    console.error('Installation failed!');
    console.error('Installation of microservice must be done by root. Try running:');
    console.error('> sudo npm install --global <package> --unsafe-perm');
    process.exit(1);
}

// Read the service template and replace placeholders
const service = fs.readFileSync('systemd/microservice.service').toString()
    .replace(/__project_description__/g, pckg.description)
    .replace(/__project_name__/g, pckg.name)
    .replace(/__install_dir__/g, __dirname);

// Write it into provided location or fall back to default
const output = args.output || ('/etc/systemd/system/' + pckg.name + '@.service');
console.log('Writing service unit file to ' + output)
fs.writeFileSync(output, service);
