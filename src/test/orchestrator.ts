import { MicroService, RouteOptions } from 'microservice'
import { Requests } from 'ootils';
import * as assert from 'assert';
import * as mocha from 'mocha';
import * as url from 'url';
import * as request from 'supertest';
import { main } from '../index';
let service: MicroService;
const port = Math.floor(Math.random() * 8999) + 1000;

describe('orquestrator', () => {

    before(async () => {
        service = await main();
        // Establish a non-changing port and setup 2 dummy routes
        service.route('/1', (request, response) => {
            const msg = request.hostname === 'localhost' ? '1' : '2';
            response.send(msg);
        });
    })

    beforeEach(() => {
        service.stop();
        service.clearCache(true);
        service.start(port);
    });
    afterEach(() => {
        service.stop();
    });

    describe('status', () => {

        it('OK', done => {
            request(service.server).get('/status').expect(200, done);
        });
    });

    describe('register', () => {

        before(() => {
            // Setup dummy status callback, although shouldn't be neccessary for this route
            service.route('/status', (request, response) => response.send({status: 'OK', data: 'OK'}));
        })

        it('using plain request', done => {
            // Setup routes to fake a service
            const name = 'fakeservice';
            const path = '/' + Math.floor(Math.random() * 9999);
            service.route(path, (request, response) => response.send(path.substring(1)));

            // Register the route as a service
            const url_self = 'http://localhost';
            const query = url.format({query: {
                spec: null,
                name: name,
                local_path: path,
                remote_path: path,
                port: service.server.address().port,
                redirect_url: url_self
            }});
            request(service.server).get('/register' + query).expect(200, done);
        });

        it('using microservice API', done => {
            // Setup routes to fake a service
            const name = 'fakeservice';
            const path = '/' + Math.floor(Math.random() * 9999);
            service.route(path, (request, response) => response.send(path.substring(1)));

            // Register the route as a service
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            service.register(url.parse(url_self), name, path).then(res => {
                done();
            }).catch(err => {
                assert(!err, err.message);
            });
        });

        it('already registered service name with same path', done => {
            // Setup routes to fake a service
            const name = 'fakeservice';
            const path = '/' + Math.floor(Math.random() * 9999);
            service.route(path, (request, response) => response.send(path.substring(1)));

            // Register the route as a service
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            service.register(url.parse(url_self), name, path).then(() => {
                service.clearCache(true);
                service.register(url.parse(url_self), name, path).then(() => {
                    done();
                }).catch(err => {
                    assert(!err, err.message);
                });
            }).catch(err => {
                assert(!err, err.message);
            });
        });

        it('with root path', done => {
            // Setup routes to fake a service
            const name = 'fakeservice';
            const path = '/';
            service.route(path, (request, response) => response.send('Hello world'));

            // Register the route as a service
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            service.register(url.parse(url_self), name, '/').then(res => {
                done();
            }).catch(err => {
                assert(!err, err.message);
            });
        });

        it('with different local and remote paths', async () => {
            // Setup routes to fake a service
            const name = 'fakeservice';
            const path = '/' + Math.floor(Math.random() * 9999);
            service.route(path, (request, response) => response.send(path.substring(1)));

            // Register the route as a service
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            const res = await service.register(url.parse(url_self), name, {local: path, remote: '/x'});
            const res1 = await request(service.server).get(path)//.expect(200);
            console.log(res1)
            const res2 = await request(service.server).get('/service/fakeservice/x').expect(200);
            assert.equal(res1.text, res2.text);
        });
        
        it('spec received', async () => {
            // Setup routes to fake a service
            const name = 'fakeservice';
            const path = '/' + Math.floor(Math.random() * 9999);
            const spec = {
                method: 'GET',
                mandatoryQueryParameters: ['param1'],
                optionalQueryParameters: ['param2']
            };
            service.route(path, (request, response) => {
                const msg = Math.floor(Math.random() * 9999);
                response.send(msg);
            }, spec);

            // Register the route as a service
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            await service.register(url.parse(url_self), name, path);
            const row = await service.db.get("SELECT spec FROM services WHERE name = ? AND path = ?", [name, path]);
            assert.deepEqual(JSON.parse(row.spec), spec);
        });
    });

    describe('request', () => {

        it('service OK', done => {
            request(service.server).get('/service/fakeservice').expect(200, done);
        });

        it('service round robin', done => {
            // Setup routes to fake a service
            const name = 'fakeservice';
            const path = '/' + Math.floor(Math.random() * 9999);
            service.route(path, (request, response) => {
                const msg = request.hostname === 'localhost' ? '1' : '2'
                response.send(msg);
            });

            // Register the route as a service using localhost as hostname
            const query1 = url.format({query: {
                spec: null,
                name: name,
                local_path: path,
                remote_path: path,
                port: service.server.address().port,
                redirect_url: 'http://localhost'
            }});
            request(service.server).get('/register' + query1).expect(200).then(() => {

                // Register the route as a service using 127.0.0.1 as hostname
                const query2 = url.format({query: {
                    spec: null,
                    name: name,
                    local_path: path,
                    remote_path: path,
                    port: service.server.address().port,
                    redirect_url: 'http://127.0.0.1'
                }});
                return request(service.server).get('/register' + query2).expect(200);

            }).then(() => {

                // Test the newly registered services
                request(service.server).get(`/service/${name}${path}`).expect(200).then(res1 => {
                    service.clearCache(true); // clear cache between requests
                    request(service.server).get(`/service/${name}${path}`).expect(200).then(res2 => {
                        service.clearCache(true); // clear cache between requests
                        request(service.server).get(`/service/${name}${path}`).expect(200).then(res3 => {
                            assert.equal(res1.text, res3.text);
                            assert.notEqual(res1.text, res2.text);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('benchmark', () => {
        let tmplog: (level: string, ...args: any[]) => Promise<void> = null;
        beforeEach(async () => {
            tmplog = service.log;
            service.log = async (level: string, ...args: any[]) => {};
        });
        afterEach(() => {
            service.log = tmplog;
        });

        it('cached', async () => {
            const start = new Date().getTime();
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            const repetitions = 1000;
            for (var i = 0; i < repetitions; i++) {
                await Requests.get(url_self + '/1');
            }
            const totaltime = new Date().getTime() - start;
            const persecond = repetitions / (totaltime / 1000.0);
            const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
            assert(persecond > 100, msg);
            console.log(msg);
        }).timeout(20000);

        it('dummy', async () => {
            const start = new Date().getTime();
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            const repetitions = 1000;
            for (var i = 0; i < repetitions; i++) {
                await Requests.get(url_self + '/1?ts=' + new Date().getTime());
            }
            const totaltime = new Date().getTime() - start;
            const persecond = repetitions / (totaltime / 1000.0);
            const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
            assert(persecond > 100, msg);
            console.log(msg);
        }).timeout(20000);

        it('root (invalid)', async () => {
            const start = new Date().getTime();
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            const repetitions = 1000;
            for (var i = 0; i < repetitions; i++) {
                await Requests.get(url_self + '/?ts=' + new Date().getTime(), {simple: false});
            }
            const totaltime = new Date().getTime() - start;
            const persecond = repetitions / (totaltime / 1000.0);
            const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
            assert(persecond > 100, msg);
            console.log(msg);
        }).timeout(20000);

        it('status', async () => {
            const start = new Date().getTime();
            const url_self = 'http://127.0.0.1:' + service.server.address().port;
            const repetitions = 1000;
            for (var i = 0; i < repetitions; i++) {
                await Requests.get(url_self + '/status?ts=' + new Date().getTime());
            }
            const totaltime = new Date().getTime() - start;
            const persecond = repetitions / (totaltime / 1000.0);
            const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
            assert(persecond > 100, msg);
            console.log(msg);
        }).timeout(20000);
    });
});